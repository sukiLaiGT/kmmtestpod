Pod::Spec.new do |spec|
    spec.name         = "SharedFramework"
    spec.version      = "1.0.7"
    spec.summary      = "A brief description of SharedFramework project."
    spec.description  = <<-DESC
    An extended description of SharedFramework project.
    DESC
    spec.homepage     = "https://www.google.com"
    spec.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2018
                   Permission is granted to...
                  LICENSE
                }

    spec.author             = { "sin man lai" => "suki.sm.lai@gtomato.com" }
    spec.source       = { :http => "https://www.dropbox.com/s/ofsez0bngx8fewh/Shared.xcframework.zip?dl=1" }
    spec.vendored_frameworks = "Shared.xcframework"
    spec.platform = :ios
    spec.ios.deployment_target  = '14.1'

    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end